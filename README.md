# SWP23 - Analysis of Synchronization Behaviors of Weakly Electric Fish

## Data Exploration

This repository is intended to explore the available data and get familiar with it.

- Data used to train the EOD localization model (https://git.imp.fu-berlin.de/bioroboticslab/robofish/electrofish/mormyrus-localization)

### 1. Download Data

Get the data from the NAS:

- EOD localization model training data: under /storage/electrofish/eod_localization/ (joined dataset can be used directly)

### 2. Methods

### 3. Main Findings
